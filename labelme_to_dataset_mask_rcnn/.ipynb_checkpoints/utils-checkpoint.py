import io
import cv2
import math
import json
import os
import glob
import argparse
import base64
import json
import warnings
import yaml
import shutil
import numpy as np
import PIL
from PIL import Image
from tqdm import trange
from labelme import utils
from multiprocessing import Process, Queue
from matplotlib import pyplot as plt

def rotate(origin, point, angle):
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def img_to_b64(img):
    f = io.BytesIO()
    img.save(f, format='PNG')
    img_bin = f.getvalue()
    img_b64 = base64.encodebytes(img_bin).decode('utf-8')
    return img_b64

def rotate_train_data_worker(q):
    while True:
        job = q.get()
        
        if job is None:
            break
        
        file = job["file"]
        
        json_file = file
        img_file = file.replace(".json", ".jpg")

        img = Image.open(img_file)

        width, height = img.size
        center = (width/2, height/2)

        with open(json_file) as f:
            labelme_json = json.load(f)

        for degree in range(5, 360, 20):

            new_labelme_json = labelme_json

            img = img.rotate(-degree, center=center)
            img.save(img_file.replace(".jpg", "-%d.jpg"%degree))

            new_labelme_json['imageData'] = img_to_b64(img)

            for si, shape in enumerate(new_labelme_json['shapes']):
                for pi, point in enumerate(shape['points']):
                    labelme_json['shapes'][si]['points'][pi] = rotate(center, point, degree*math.pi/180)

            with open(json_file.replace(".json", "-%d.json"%degree), 'w') as f:
                json.dump(new_labelme_json, f)

def rotate_test_data_worker(q):
    while True:
        job = q.get()
        
        if job is None:
            break
        
        file = job["file"]
        
        img_file = file

        img = Image.open(img_file)

        width, height = img.size
        center = (width/2, height/2)

        for degree in range(5, 360, 60):

            img = img.rotate(-degree, center=center)
            img.save(img_file.replace(".jpg", "-%d.jpg"%degree))


def resize_train_data_worker(q):
    while True:
        job = q.get()
        
        if job is None:
            break
        
        file = job["file"]
        
        json_file = file
        img_file = file.replace(".json", ".jpg")

        img = Image.open(img_file)
        width, height = img.size
        img = img.resize([width//10, height//10])
        img.save(img_file.replace("output_dataset", "resized_output_dataset"))

        with open(json_file) as f:
            labelme_json = json.load(f)

        labelme_json['imageData'] = img_to_b64(img)

        for si, shape in enumerate(labelme_json['shapes']):
            for pi, point in enumerate(shape['points']):
                labelme_json['shapes'][si]['points'][pi] = [int(p//10) for p in point]

        with open(json_file.replace("output_dataset", "resized_output_dataset"), 'w') as f:
            json.dump(labelme_json, f)        


def resize_test_data_worker(q):
    while True:
        job = q.get()
        
        if job is None:
            break
        
        file = job["file"]
        
        img_file = file

        img = Image.open(img_file)
        width, height = img.size
        img = img.resize([width//10, height//10])
        img.save(img_file.replace("output_dataset", "resized_output_dataset"))
            
            
def labelme_worker(q):
    while True:
        job = q.get()
        
        if job is None:
            break
        
        json_file = job["json_file"]
        output_path = job["output_path"]

        out_dir = os.path.basename(json_file).replace('.', '_')
        out_dir = os.path.join(output_path, "labelme_json", out_dir)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)

        data = json.load(open(json_file))

        if data['imageData']:
            imageData = data['imageData']
        else:
            imagePath = os.path.join(os.path.dirname(json_file), data['imagePath'])
            with open(imagePath, 'rb') as f:
                imageData = f.read()
                imageData = base64.b64encode(imageData).decode('utf-8')
        img = utils.img_b64_to_arr(imageData)

        label_name_to_value = {'_background_': 0}
        for shape in data['shapes']:
            label_name = shape['label']
            if label_name in label_name_to_value:
                label_value = label_name_to_value[label_name]
            else:
                label_value = len(label_name_to_value)
                label_name_to_value[label_name] = label_value

        label_values, label_names = [], []
        for ln, lv in sorted(label_name_to_value.items(), key=lambda x: x[1]):
            label_values.append(lv)
            label_names.append(ln)
        assert label_values == list(range(len(label_values)))

        lbl = utils.shapes_to_label(img.shape, data['shapes'], label_name_to_value)

        captions = ['{}: {}'.format(lv, ln)
                    for ln, lv in label_name_to_value.items()]
        lbl_viz = utils.draw_label(lbl, img, captions)

        PIL.Image.fromarray(img).save(os.path.join(out_dir, 'img.png'))
        utils.lblsave(os.path.join(out_dir, 'label.png'), lbl)
        PIL.Image.fromarray(lbl_viz).save(os.path.join(out_dir, 'label_viz.png'))

        with open(os.path.join(out_dir, 'label_names.txt'), 'w') as f:
            for lbl_name in label_names:
                f.write(lbl_name + '\n')

        # warnings.warn('info.yaml is being replaced by label_names.txt')
        info = dict(label_names=label_names)
        with open(os.path.join(out_dir, 'info.yaml'), 'w') as f:
            yaml.safe_dump(info, f, default_flow_style=False)

        # print('Saved to: %s' % out_dir)