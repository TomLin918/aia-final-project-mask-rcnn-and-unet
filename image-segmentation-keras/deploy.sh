#!/bin/bash

# chmod u+x ./deploy.sh


# Push kernel
tput setaf 4 && echo -e "========== Push kernel ? (y/n)" && read answer && tput sgr 0;

if [ "$answer" == 'y' ]; then
    kaggle kernels push
fi

# Create dataset
tput setaf 4 && echo -e "========== Create dataset ? (y/n)" && read answer && tput sgr 0;

if [ "$answer" == 'y' ]; then
    mkdir deploy_tmp
    zip -r deploy_tmp/datasets.zip dataset
    cp dataset-metadata.json deploy_tmp/dataset-metadata.json

    tput setaf 4 && echo -e "========== Is Init ? (y/n)" && read answer && tput sgr 0;
    if [ "$answer" == 'y' ]; then
        kaggle datasets create -p deploy_tmp
    else
        kaggle datasets version -m '-' -p deploy_tmp -d
    fi

    rm -rf deploy_tmp
fi
